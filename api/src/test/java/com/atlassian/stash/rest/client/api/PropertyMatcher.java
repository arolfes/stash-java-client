package com.atlassian.stash.rest.client.api;

import com.google.common.base.Preconditions;
import com.google.common.collect.Maps;
import org.hamcrest.BaseMatcher;
import org.hamcrest.Description;
import org.hamcrest.Matcher;

import java.beans.BeanInfo;
import java.beans.FeatureDescriptor;
import java.beans.IntrospectionException;
import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class PropertyMatcher<T> extends BaseMatcher<T> {
    private final Map<String, Matcher<?>> expectedPropertyMatchers;
    private Set<String> mismatchedProperties = new HashSet<>();

    public PropertyMatcher(Map<String, Matcher<?>> expectedPropertyMatchers) {
        this.expectedPropertyMatchers = expectedPropertyMatchers;
    }

    private Map<String, PropertyDescriptor> getPropertyDescriptors(Class<?> cls) {
        try {
            BeanInfo beanInfo = Introspector.getBeanInfo(cls);
            return Maps.uniqueIndex(
                    Arrays.asList(beanInfo.getPropertyDescriptors()),
                    FeatureDescriptor::getName
            );
        } catch (IntrospectionException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public boolean matches(Object o) {
        try {
            if (o == null) {
                return false;
            }
            Map<String, PropertyDescriptor> propertyDescriptors = getPropertyDescriptors(o.getClass());
            for (Map.Entry<String, Matcher<?>> fieldEntry : expectedPropertyMatchers.entrySet()) {
                String name = fieldEntry.getKey();
                Matcher<?> matcher = fieldEntry.getValue();
                final PropertyDescriptor propertyDescriptor = Preconditions.checkNotNull(propertyDescriptors.get(name),
                        "Property <%s> not found on <%s> class", name, o.getClass());
                Object propertyValue = propertyDescriptor.getReadMethod().invoke(o);
                if (!matcher.matches(propertyValue)) {
                    mismatchedProperties.add(name);
                }
            }
            return mismatchedProperties.isEmpty();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }



    @Override
    public void describeTo(Description description) {
        int i = 0;
        description.appendText("[properties: ");
        for (Map.Entry<String, Matcher<?>> fieldEntry : expectedPropertyMatchers.entrySet()) {
            String name = fieldEntry.getKey();
            Matcher<?> matcher = fieldEntry.getValue();
            description.appendText((i > 0 ? ", " : "") + "'" + name + "'");
            if (mismatchedProperties.contains(name)) {
                description.appendText("(MISMATCHED)");
            }
            description.appendText(" ");
            matcher.describeTo(description);
            i++;
        }
        description.appendText("]");
    }
}