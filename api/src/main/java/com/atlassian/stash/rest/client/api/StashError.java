package com.atlassian.stash.rest.client.api;

import com.google.common.base.Objects;

import javax.annotation.Nullable;

/**
 * Describes an Stash error cause
 */
public class StashError {
    private final String message;
    private final String context;
    private final String exceptionName;

    public StashError(String message, String context, String exceptionName) {
        this.context = context;
        this.message = message;
        this.exceptionName = exceptionName;
    }

    /**
     * @return name of field with problem, null if it is not a field error
     */
    @Nullable
    public String getContext() {
        return context;
    }

    /**
     * @return the error message
     */
    @Nullable
    public String getMessage() {
        return message;
    }

    /**
     * @return the error exception name as provided by Stash
     */
    @Nullable
    public String getExceptionName() {
        return exceptionName;
    }

    @Override
    public String toString() {
        return Objects.toStringHelper(this)
                .add("context", context)
                .add("message", message)
                .add("exceptionName", exceptionName)
                .toString();
    }
}
