package com.atlassian.stash.rest.client.core.entity;


import com.google.gson.JsonObject;

public class StashUserSshKeyRequest {
    private final String label;
    private final String publicKey;

    public StashUserSshKeyRequest(final String label, final String publicKey) {
        this.label = label;
        this.publicKey = publicKey;
    }

    public JsonObject toJson() {
        JsonObject key = new JsonObject();
        key.addProperty("label", label);
        key.addProperty("text", publicKey);

        return key;
    }
}
